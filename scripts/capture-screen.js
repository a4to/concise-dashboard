
    let mediaRecorder;
    let chunks = [];
    let isRecording = false;

    document.getElementById('toggleRec').addEventListener('click', async () => {
      if (!isRecording) {
        const stream = await navigator.mediaDevices.getDisplayMedia({ video: true });
        mediaRecorder = new MediaRecorder(stream);

        mediaRecorder.ondataavailable = e => chunks.push(e.data);
        mediaRecorder.onstop = () => {
          const blob = new Blob(chunks, { type: 'video/webm' });
          const url = URL.createObjectURL(blob);
          document.getElementById('preview').src = url;

          // Auto-save video
          const a = document.createElement('a');
          a.style.display = 'none';
          a.href = url;
          var name = (new Date().toLocaleDateString('en-GB', {
            day: '2-digit',
            month: 'short'
          }).replace(/ /g, '') + '-' + new Date().toLocaleTimeString('en-GB', {
            hour: '2-digit',
            minute: '2-digit'
          }).replace(/:/g, '.')) + '.webm';


          a.download = name;
          document.body.appendChild(a);
          a.click();
          setTimeout(() => {
            document.body.removeChild(a);
            URL.revokeObjectURL(url);
          }, 100);
        };

        mediaRecorder.start();
      } else {
        mediaRecorder.stop();
      }
      isRecording = !isRecording;
    });



