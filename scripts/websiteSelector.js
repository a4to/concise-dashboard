
const sendNotification = document.getElementById('websiteSelector');
const urls = {};

document.addEventListener('DOMContentLoaded', async () => {
  const URLs = await fetch('/scripts/URLs.json').then(response => response.json());
    Object.keys(URLs).forEach(url => {
      urls[URLs[url]] = url;
    });
  console.log(urls);
});

websiteSelector.addEventListener('click', async () => {

  const site = await Swal.fire({
    title: 'Select Website',
    input: 'select',
    inputOptions: urls,
    inputPlaceholder: 'Select a Website',
    showCancelButton: true,
    inputValidator: (value) => {
      return new Promise((resolve) => {
        if (value !== '') {
          resolve()
        } else {
          resolve('You need to select a Website')
        }
      })
    }
  }).then((result) => {
    if (result.isConfirmed) {
      const url = result.value;
      window.open(url, '_blank');
    }
  })
});

